#include <iostream>
#include <string>
#include "Shift3Text.h"
#include "ShiftText.h"
#include "DecryptException.h"
using namespace std;

Shift3Text::Shift3Text(string text) : ShiftText(text, _key) //the c'tor function
{
	text = _text;
	_key = 3;
}
Shift3Text::~Shift3Text() //the d'tor function
{
	_text = nullptr;
	_key = 0;
}
void Shift3Text::decrypt() //this function checking if the text is encrypted and if it is she uses the function decrypt in ShiftText
{
	if (_isEnc != true)
	{
		throw DecryptException();
	}
	ShiftText::decrypt(_key);
}