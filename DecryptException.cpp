#include <iostream>
#include <string>
#include <exception>
#include "DecryptException.h"

const char* DecryptException::what() const throw()
{
	return "Error! text isn't encrypted";
}