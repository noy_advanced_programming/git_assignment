#ifndef _PLAINTEXT_H
#define _PLAINTEXT_H
#include <string>
using namespace std;

class PlainText
{
protected:
	string _text;
	bool _isEnc;

public:
	PlainText(string text);
	~PlainText();
	bool isEnc();
	string getText();
};

#endif
