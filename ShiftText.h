#ifndef _SHIFTTEXT_H
#define _SHIFTTEXT_H
#define SMALL_Z 122
#define SMALL_A 97
#include <string>
#include "PlainText.h"
using namespace std;

class ShiftText : public PlainText
{
public:
	ShiftText(string text, int key);
	~ShiftText();
	void decrypt(int key);
protected:
	int _key;
private:
	string encrypt(string text, int key);
};

#endif