#include <iostream>
#include <string>
#include "ShiftText.h"
#include "DecryptException.h"
using namespace std;

ShiftText::ShiftText(string text, int key) : PlainText(text) //the c'tor function
{
	_text = text;
	_key = key;
}
ShiftText::~ShiftText() //the d'tor function
{
	_text = nullptr;
	_key = 0;
}
void ShiftText::decrypt(int key) //this functionn checking if the text is encrypted and if it is she decrypted the text.
{
	if (_isEnc != true)
	{
		throw DecryptException();
	}
	int diff;
	for (int i = 0; i < _text.length(); i++)
	{
		if (!(isalpha(_text[i] - key))) //if its not a letter
		{
			diff = SMALL_A - (_text[i] - key);
			_text[i] = SMALL_Z - diff + 1;
		}
		else
		{
			_text[i] = _text[i] - key;
		}
	}
	_isEnc = false;
}
string ShiftText::encrypt(string text, int key) //this function encrypts the text she got from the user with the key.
{
	int diff;

	for (int i = 0; i < text.length(); i++)
	{
		if (!(isalpha(text[i] + key)))
		{
			diff = (text[i] + key) - SMALL_Z;
			text[i] = SMALL_A + diff - 1;
		}
		else
		{
			text[i] = text[i] + key;
		}
	}
	_text = text;
	_isEnc = true;
	return text;
}