#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include "SubstitutionText.h"
#include "PlainText.h"
#include "DecryptException.h"
using namespace std;

SubstitutionText::SubstitutionText(string text, fstream& dictionary) :PlainText(text) //the c'tor
{
	text = encrypt(text, dictionary);
}
SubstitutionText::~SubstitutionText()//the d'tor
{
	_text = nullptr;
}
string SubstitutionText::encrypt(string text, fstream& dictionary) //this function encrypts the text by switch all the string's letters to the letters in the dictionary 
{
	ifstream myFile("dictionary.csv");
	string letters = "", letter = "";
	int i = 0;
	if (!myFile.is_open())
	{
		cout << "Can't open file" << "\n";
	}
	while (myFile.is_open() && i<27)
	{
		getline(myFile, letter, ',');
		letters = letters +letter; //create an array of the letters in the dictionary - if i%2==0 so letters[i] is the original letter and if i&2!=0 so letters[i] is the letter that show in the dictionary
		i++;
	}
	int j =0;
	myFile.close();
	letters.erase(std::remove(letters.begin(), letters.end(), '\n'), letters.end()); //delete the \n from the array
	for (i = 0; i < text.length(); i++)
	{
		for (j = 0; j < letters.length(); j++)
		{
			if (j % 2 == 0)
			{
				if (letters[j] == text[i])
				{
					text[i] = letters[j+1];
					break;
				}
			}	
		}
	}
	_isEnc = true;
	_text = text;
	return _text;
}
void SubstitutionText::decrypt(fstream& dictionary) //this function decrypts the text by switch all the string's encrypted letters to the original letters 
{
	if (_isEnc != true)
	{
		throw DecryptException();
	}
	string text = _text;
	ifstream myFile("dictionary.csv");
	string letters = "", letter = "";
	int i = 0;
	if (!myFile.is_open())
	{
		cout << "Can't open file" << "\n";
	}
	while (myFile.is_open() && i<27)
	{
		getline(myFile, letter, ','); 
		letters = letters + letter; ////create an array of the letters in the dictionary - if i%2==0 so letters[i] is the original letter and if i&2!=0 so letters[i] is the letter that show in the dictionary
		i++;
	}
	int j = 0;
	myFile.close();
	letters.erase(std::remove(letters.begin(), letters.end(), '\n'), letters.end()); //delete the \n from the array
	for (i = 0; i < text.length(); i++)
	{
		for (j = 0; j < letters.length(); j++)
		{
			if (j % 2 != 0)
			{
				if (letters[j] == text[i])
				{
					text[i] = letters[j - 1];
					break;
				}
			}
		}
	}
	_text = text;
	_isEnc = false;
}