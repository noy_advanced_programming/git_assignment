#ifndef _SHIFT3TEXT_H
#define _SHIFT3TEXT_H
#include <string>
#include "ShiftText.h"
using namespace std;

class Shift3Text : public ShiftText
{
public:
	Shift3Text(string text);
	~Shift3Text();
	void decrypt();
};

#endif