#include "ShiftText.h"
#include "SubstitutionText.h"
#include "Shift3Text.h"
#include "DecryptException.h"
#include "PlainText.h"
#include <iostream>
#include <fstream>

#define SHIFTING_KEY 10
#define DICTIONARY_PATH "dictionary.csv"

using namespace std;

void Alice();
PlainText Bob(SubstitutionText msg);
PlainText Bob(ShiftText msg);
PlainText Bob(Shift3Text msg);

/*
This is the main function of the program.
It starts the conversation between Alice and Bob
*/
int main()
{
	Alice();

	cout << endl;
	system("PAUSE");
	return 0;
}

/*
This is Alice's part in the conversation.
It gets a legal string from the user ans sends it to Bob in
three different kinds of encryptions.
It also prints every response from Bob.
*/
void Alice()
{
	fstream dict;
	dict.open(DICTIONARY_PATH);
	string msg = "";
	cout << "Alice" <<  '\n' << "Enter a message to send to Bob:"<< '\n';
	cin >> msg;
	cout << "\n";
	cout << "Sending in SubstitutionText mode..." << '\n';
	SubstitutionText substitutionText(msg, dict);
	Bob(substitutionText);
	cout << "Sending in Sift mode..." << '\n';
	ShiftText shiftMode(msg, SHIFTING_KEY);
	Bob(shiftMode);
	cout << "Sending in Shift-3 mode..." << '\n';
	Shift3Text shift3Mode(msg);
	Bob(shift3Mode);
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Substitution mode
*/
PlainText Bob(SubstitutionText msg)
{
	cout << "Bob:" << '\n';
	fstream dict;
	dict.open(DICTIONARY_PATH);
	string textMsg = msg.getText();
	cout << "The recieved message is: " << textMsg << '\n';
	msg.decrypt(dict);
	textMsg = msg.getText();
	cout << "the decrypted message is: " << textMsg << '\n';
	string textBack = "Thank you Alice!";
	return textBack;
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift mode
*/
PlainText Bob(ShiftText msg)
{
	cout << "Bob:" << '\n';
	fstream dict;
	dict.open(DICTIONARY_PATH);
	string textMsg = msg.getText();
	cout << "The recieved message is: " << textMsg << '\n';
	msg.decrypt(SHIFTING_KEY);
	textMsg = msg.getText();
	cout << "the decrypted message is: " << textMsg << '\n';
	string textBack = "Thank you again Alice!";
	return textBack;
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift-3 mode
*/
PlainText Bob(Shift3Text msg)
{
	cout << "Bob:" << "\n";
	fstream dict;
	dict.open(DICTIONARY_PATH);
	string textMsg = msg.getText();
	cout << "The recieved message is: " << textMsg << "\n";
	msg.decrypt();
	textMsg = msg.getText();
	cout << "the decrypted message is: " << textMsg << "\n";
	string textBack = "Many Thanks Alice!";
	return textBack;
}