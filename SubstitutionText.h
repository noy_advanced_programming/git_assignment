#ifndef _SUBSTITUTIONTEXT_H
#define _SUBSTITUTIONTEXT_H
#include <string>
#include "PlainText.h"
using namespace std;

class SubstitutionText : public PlainText
{
public:
	SubstitutionText(string text, fstream& dictionary);
	~SubstitutionText();
	void decrypt(fstream& dictionary);
private:
	string encrypt(string text, fstream& dictionary);
};

#endif