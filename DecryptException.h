#pragma once
#include <iostream>
#include <string>
#include <exception>

class DecryptException : public std::exception
{
public:
	virtual const char* what() const throw();
};

