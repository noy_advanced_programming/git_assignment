#include <iostream>
#include <string>
#include "PlainText.h"
#include "DecryptException.h"
using namespace std;

PlainText::PlainText(string text) //the c'tor function
{
	_text = text;
	_isEnc = false;
}
PlainText::~PlainText() //the d'tor function
{
	delete &_text;
}
bool PlainText::isEnc() //this function return if the text is encrypt or not
{
	return _isEnc;
}
string PlainText::getText() //this function return the text.
{
	return  _text;
}
